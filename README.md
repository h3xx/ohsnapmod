# Oh Snap modded

## a bitmap font

This font was created by taking the [ohsnap](https://sourceforge.net/projects/osnapfont/) font and, using glyphs from [terminus](http://terminus-font.sourceforge.net/), patching the holes in the ISO8859-1 version in order to make it more complete with regard to special graphics mode.

## Demo

![7x12n](../../raw/flair/images/7x12n.png)
![7x12b](../../raw/flair/images/7x12b.png)
![7x14n](../../raw/flair/images/7x14n.png)
![7x14b](../../raw/flair/images/7x14b.png)
